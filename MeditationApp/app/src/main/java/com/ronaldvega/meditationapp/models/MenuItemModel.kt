package com.ronaldvega.meditationapp.models

import android.graphics.drawable.Icon
import androidx.annotation.DrawableRes

data class MenuItemModel(
    val title: String,
    @DrawableRes val iconId: Int
)
